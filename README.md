# This depository contains the Matlab source code I developed as part of my first short DTC project, which is about the computational modelling of biofilms. This Matlab prototype was used to test the "shortest distance" mechanics (SDM) for line-segment representations of non-circular cells. 

# You can watch videos of the code in action at http://www.youtube.com/channel/UCeU97X_UzGBZZ50oWGokG0Q, or use the code's movie-making functionality to make your own.
