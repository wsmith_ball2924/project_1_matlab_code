function MultiLineSimulation ()

% MULTILINESIMULATION is a simple MATLAB prototype to test "shortest distance"
%mechanics (SDM) for line-segment representations of non-circular cells.

% ### Model description

% Each cell is represented by a line segment, an instance of the class
% "Segment". Forces between pairs of segments are calculated by finding the
% vector of closest approach between them, then applying linear spring forces
% along those vectors. Torques on lines are also calculated by examining
% the point of contact between the separation vector and the line. Forces
% and Torques are summed, and then used to update segment positions and
% angles using an [overdamped] finite difference method.

% ### What the plots mean

% Each cell is shown as a black line segment. An ellipse is drawn around the 
% line for visualisation purposes. Boundary segments, which are like normal 
% segments but which never move, are shown in blue. Dashed magenta lines show the
% closest approach vector between pairs of segments, and red crosses
% highlight the contact point. Forces exerted at contact points are shown
% in green. Forces exerted on cell centres are not shown.

% (c)=[ CC-SA-BY 3.0 ]. Will Smith @ 2013.

    
    %% Parameters

% sim params
int_iter = 30;   
int_NoCells = 3;
int_NoBoundaries = 0;
double_timestep = 0.01;
BoolWriteToFile = false;
BoolPlotForces = true;


% mechanical params
double_gammaM = 1.0; % resistance to linear motion
double_gammaI = 1.0; % resistance to rotational motion
EqmSeparation = 4.0; % separation at which forces are zero.
HookeConstant = 1.0; % linear spring coefficient
ForceCutOff = 4.0;    % separation at which forces are cut off


    %% Initialisation and boundary conditions

% Create an empty array of segment objects
NodeList = Segment.empty(int_NoCells+int_NoBoundaries, 0);

%fill the array with instances
for n=1:int_NoCells+int_NoBoundaries
    NodeList(n).index = n;
end

% Initialise each node.

    % OPTION 1: automatic
    %RandomInitialisation(NodeList)
    
    % OPTION 2: manual  (Index, Angle, Length, Centre)
    NodeList(1).InitialiseSegment( 1, pi/2, 1.0, [-0.7,0,0] );
    NodeList(2).InitialiseSegment( 2, pi/2, 1.0, [1,2,0] ); 
    NodeList(3).InitialiseSegment( 3, pi/2, 1.0, [1,-2,0] );
        % add further cells required ... make sure to change "int_NoCells"
    %NodeList(4).InitialiseSegment( 4, 0.1, 1.0, [-1,-2,0] );
 
% Initialise each wall in a 10x10 box.  (Index, Angle, Length, Centre)

    %NodeList(int_NoCells+1).InitialiseSegment( 1, pi/2, 5.0, [-5,0,0] );  % Left
    %NodeList(int_NoCells+2).InitialiseSegment( 2, pi/2, 5.0, [+5,0,0] );  % Right
    %NodeList(int_NoCells+3).InitialiseSegment( 3, 0.0,  5.0, [0,-5,0] );  % Bottom
    %NodeList(int_NoCells+4).InitialiseSegment( 4, 0.0,  5.0, [0,+5,0] );  %Top


    %% iteration
    
    close
    Fig = figure(1);
    set(Fig, 'Position', [0 0 500 500])

for i=1:int_iter
        clf
        axis([-4 4 -2 4]);
        axis square
        axis equal
        hold on % moved upstream to accommodate force plotting
        grid on
        
        %% Calulate net forces and torques on each segment
        for j=1:int_NoCells
            
            % choose current cell and reset Force and Torque to zero.
            % NB: the current cell is never a wall, but a *neighbouring* cell might
            % be, so the loop indices differ by 4.
            
            CurrentSegment = NodeList(j);
            CurrentSegment.ResetForceAndTorque;
            
            % For a given segment, find shortest distances to all other
            % segments. Then calculate force and torque contributions.
            
            g=0;
            for k=1:int_NoCells+int_NoBoundaries-1 % the minus 1 avoids double counting
                
                if k==j   % Current and Neighbour can't be the same...
                   g=g+1; % So skip to the next node permanently (avoid double reading)
                end
                
                CurrentNeighbour = NodeList(k+g);
                
                % Calculating shortest distances (1 is Current, 2 is Neighbour)
                [vec_orientation1] = CurrentSegment.GetOrientationVector;
                [vec_orientation2] = CurrentNeighbour.GetOrientationVector;  
                [vec_tip1, vec_tail1] = GetLine(CurrentSegment.vec_centre, vec_orientation1, CurrentSegment.length);
                [vec_tip2, vec_tail2] = GetLine(CurrentNeighbour.vec_centre, vec_orientation2, CurrentNeighbour.length);
                % If cells are of uniform length:
                	%[vec_MinSep, vec_Contact] = GetSeparationAndContact(vec_tip1, vec_tail1, vec_tip2,vec_tail2);
                % OR if cells are of different lengths
                    [vec_MinSep, vec_Contact] = GetSeparationAndContactVSL(vec_tip1, vec_tail1, vec_tip2, vec_tail2);
                    
                
                % Calculate force and torque contributions, and add them on
                vec_Force = GetForce(vec_MinSep, EqmSeparation, HookeConstant, ForceCutOff);
                double_Torque = GetTorque(vec_Force, CurrentSegment.vec_centre, vec_Contact);
                CurrentSegment.AddForceAndTorqueContributions(vec_Force, double_Torque);
                
                
                % Plot separations and forces between non-boundary line pairs
                if j <= int_NoCells && BoolPlotForces 
                    PlotForcesAndNearestDistances(vec_Contact, vec_MinSep, vec_Force)  
                end % Force plotting

            end % Neighbour iteration
                   
       end % Cell iteration
       
       %% Movement and plotting
       
       for l=1:int_NoCells % the walls are in the node list, but never get updated
           
            % Movement
            CurrentSegment = NodeList(l);
            CurrentSegment.vec_centre = CurrentSegment.vec_centre ...
                                      + ( double_timestep / double_gammaM ) * CurrentSegment.vec_force;

            CurrentSegment.angle = CurrentSegment.angle ...
                                      + (double_timestep / double_gammaI) * CurrentSegment.double_torque; 
            
            % Plotting
            PlotSegment(CurrentSegment)         % plot the segment
            PlotCellEllipses(CurrentSegment)    % plot a fetching ellipse around it
            %PlotBoundaryConditions(NodeList, int_NoBoundaries)    % plot the wall segments

            
        end % cell iteration
        hold off
        %xlabel('x coordinate', 'FontName', 'Arial', 'FontSize', 18)
        %ylabel('y coordinate', 'FontName', 'Arial', 'FontSize', 18)
        %title('SDM Prototype in MATLAB^{\(R)}','FontName', 'Arial', 'FontSize', 24)
        
        % making movies
        F = getframe();
        if BoolWriteToFile
            WriteToImageFile(F, i);
        end
 
    end % time iteration
    
end % simulation


    

%% Aux functions

function [LengthSquared] = GetSquareLength(vecA, vecB)
% GETSQUARELENGTH returns the distance between two points given by a pair
% of vectors vecA and vecB
Segment = vecB - vecA;
LengthSquared = Segment(1)^2 + Segment(2)^2 + Segment(3)^2;
end

function [LengthSquared] = GetSquareLengthVector(vecA)
% GETSQUARELENGTHOFVECTOR returns the magnitude of vector vecA.
LengthSquared = (vecA(1)^2 + vecA(2)^2 + vecA(3)^2);
end
 
function [vec_tip, vec_tail] = GetLine(vec_centre, vec_orientation, double_length)
% GETLINE finds returns vectors for the tip and tail of a line segment,
% given that segment's (unit) orientation vector, length and centre
% coordinate.
    vec_tip = vec_centre + double_length*vec_orientation;
    vec_tail = vec_centre - double_length*vec_orientation;
end

function [X,Y] = GetLinePoints(vec_tip, vec_tail)
% GETLINEPOINTS returns a vector of X and Y coordinates from the segment
% connecting points vec_tip and vec_tail, to be used for plotting.
    X = linspace(vec_tip(1), vec_tail(1), 2);
    Y = linspace(vec_tip(2), vec_tail(2), 2);
end

function [vec_Force] = GetForce(vec_MinSep, EqmSeparation, HookeConstant, ForceCutOff)
% GETFORCE calculates the force vector given the minimum separation vector
% and the equilibrium separation. 

    if norm(vec_MinSep) < ForceCutOff

        vec_Force = HookeConstant * vec_MinSep / norm(vec_MinSep)...
                       * (EqmSeparation-norm(vec_MinSep));       
    else
        vec_Force = [0,0,0];
    end
                    
end

function [double_Torque] = GetTorque(vec_Force, vec_centre, vec_Touch)
% GETTORQUE calculates the torque vector given the force and the contact
% point.

    vec_Torque = cross (vec_Touch - vec_centre, vec_Force);
    Sign = sign(vec_Torque(3)); % turns double_torque into a pseudovector
    double_Torque = Sign * (vec_Torque(1)^2 + vec_Torque(2)^2 + vec_Torque(3)^2)^0.5;
end

function PlotBoundaryConditions(NodeList, int_NoBoundaries)
% PLOTBOUNDARYCONDITIONS unpacks the static boundary nodes from the node
% list and plots them on the sim visualiser. It's a bit wasteful because it
% has to recalculate positions every iteration, when they are not changing.

    NoNodes = length(NodeList);
    for i=(NoNodes-int_NoBoundaries+1):(NoNodes)
        
            CurrentSegment = NodeList(i);
            [vec_tip, vec_tail] = GetLine(CurrentSegment.vec_centre, CurrentSegment.GetOrientationVector, CurrentSegment.length);
            [X,Y] = GetLinePoints(vec_tip, vec_tail);

            plot(X,Y, '-')    
    end

end

function PlotSegment(CurrentSegment)
% PLOTSEGMENT Plots the current position and orientation of a line segment

        [vec_tip, vec_tail] = GetLine(CurrentSegment.vec_centre, CurrentSegment.GetOrientationVector, CurrentSegment.length);
        [X,Y] = GetLinePoints(vec_tip, vec_tail);
        
        plot(X,Y, '-','LineWidth',2,'color','k')
                            
end

function PlotCellEllipses(CurrentSegment)
% PLOTCELLELLIPSES plots an elipse around a segment, for visualisation
% purposes, using a parametric method. 
    
    centre = CurrentSegment.vec_centre;
    theta =  CurrentSegment.angle;
    a = 1.5*CurrentSegment.length; % primary axis length
    b = 0.5*a;                 % secondary axis length

    syms phi
    phi = 0:0.1:2.01*pi;
    x = centre(1) + a*b*cos(phi + theta).*(b^2*(cos(phi)).^2 + a^2*(sin(phi)).^2).^-0.5;
    y = centre(2) + a*b*sin(phi + theta).*(b^2*(cos(phi)).^2 + a^2*(sin(phi)).^2).^-0.5;    
    plot(x,y);

end

function PlotForcesAndNearestDistances(vec_Contact, vec_MinSep, vec_Force)
% PLOTFORCESANDNEARESTDISTANCES plots individual force contributions for
% line pairs, as well as contact points and nearest-approach vectors.
% It's quite expensive, so mainly used for debugging and testing.

        [CX,CY] = GetLinePoints(vec_Contact, (vec_Contact - vec_MinSep)); % Contact vector
        [FX,FY] = GetLinePoints(vec_Contact, (vec_Contact - vec_Force));  % Force vector  
        plot(FX, FY, '<-','color', 'g', 'LineWidth', 3) 
        plot(CX,CY,'--','color', 'm', 'LineWidth', 1)
        plot(vec_Contact(1), vec_Contact(2), 'x', 'color', 'r','LineWidth', 3)

end

function [vec_MinSep, vec_Contact] = GetSeparationAndContact(H1, T1, H2, T2)
% GETSEPARATIONANDCONTACT provides an alternative method for getting
% minumum separation and contact vectors from a pair of line segments, S1
% and S2, respectively defined by vectors to points [H1,T1] and [H2,T2].

% this adapted from SoftSurfers code, where P0==T1 and P1==H1.
% You can see his derivation at http://geomalgorithms.com/a07-_distance.html

% The object is to calculate sc, tc first and then use these to decide
% which path to return (more on this later when I understand more...)

% ///// DEBUGGED AND TESTED WPJS 20.06.13

u = H1 - T1; % vector for first line
v = H2 - T2; % vector for second line
w = T1 - T2; % separation of the tails

% the key dot products
a = dot(u,u);   % always >= 0
b = dot(u,v);                      
c = dot(v,v);   % always >= 0
d = dot(u,w);
e = dot(v,w);
%============
D = a*c - b*b;  % always >= 0

SMALL_NUM = 0.00000001; %was 0.00000001

% set default values
sD = D;
tD = D;

% compute line parameters of the two closest points

    %///////////////////////////////////| PARALLEL CASE
    if D < SMALL_NUM % when the lines are almost parallel...
        sN = 0.0;
        tN = e;
        sD = 1.0;
        tD = c;
        % >>>> DEBUG: checked this compartment
        
    %//////////////////////////////////| NON-PARALLEL CASE
    else            %get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        % >>>> DEBUG: checked this compartment
        
        if sN < 0.0       % sc < 0 => the s=0 edge is visible
            sN = 0.0;
            tN = e;
            tD = c;
        % >>>> DEBUG: checked this compartment
        elseif sN > sD      % sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;  
        % >>>> DEBUG: checked this compartment    
        end
        % IF_ HERE Implicit: if 0.0 < sN < sD, none of these cases apply and values
        % of sN, tN remain defined as
        %    sN = (b*e - c*d);
        %    tN = (a*e - b*d);
    end

    %//////////////////////////////| INTEPRET VISIBLE EDGES IN S/T SPACE
    if tN < 0.0      %tc < 0 => the t=0 edge is visible
       tN = 0.0;
       if -d < 0.0  % recompute sc for this edge
           sN = 0.0;
       elseif -d > a
           sN = sD;
       else
           sN = -d;
           sD = a;
       end
    elseif tN > tD        % tc > 1  => the t=1 edge is visible
       tN = tD;
       if (-d + b) < 0.0  % recompute sc for this edge
           sN = 0.0;
       elseif (-d + b) > a
           sN = sD;
       else
           sN = (-d +  b);
           sD = a;
       end
    end
    
    % do the division to get sc and tc
    % =================================
    %sc = (abs(sN) < SMALL_NUM ? 0.0 : sN / sD); % need to replace this operator: it's called a ternary conditional
    %tc = (abs(tN) < SMALL_NUM ? 0.0 : tN / tD); % INSERT <bool> ? <value if true> : <value if false>
    
    % equivalent MatLab statments
    if abs(sN) < SMALL_NUM % choose sc
        sc = 0.0;
    else
        sc = sN / sD;
    end
    if abs(tN) < SMALL_NUM % choose tc
        tc = 0.0;
    else
        tc = tN / tD;
    end
    
    vec_MinSep = w + (sc * u) - (tc * v);  % =  S1(sc) - S2(tc): points TOWARDS S1
    
    if D < SMALL_NUM % check for parallel lines
        
        % if the lines are parallel, evaluate their overlap:
        % overlapping lines form a cuboid of non-zero area, whose centre is
        % P. We want to set the min_VecSep vector so that it passes through P.
        
        N1 = T1 + 0.5*(H1 - T1);      % centre of L1
        N2 = T2 + 0.5*(H2 - T2);      % centre of L2
        P = N1 + 0.5*(N2 - N1);       % centre of overlap cuboid

        
        Length = norm(H1 - T1);                      % length of the first segment, L1
        Projection = dot((P-T1),(H1-T1)) / Length^2; % projection of the line T1-P onto L1
       
        if Projection > 0.0 && Projection < 1.0
            vec_Contact = P + 0.5*vec_MinSep;
        else                                         % parallel but not overlapping case
            vec_Contact = T1 + (sc * u);
        end
        
    else % non-parallel case
         vec_Contact = T1 + (sc * u);
    end % if D < SMALL_NUM
   
end % function

function WriteToImageFile(F, index)
% WRITETOIMAGEFILE exports a frame object F as a png file, labelled by the
% iteration number "index". These files can then be compiled together to
% make a movie, using programs like mencoder (see "make_a_movie.sh")

        [X,~] = frame2im(F);
        imwrite(X, strcat('Frame', sprintf('%03d',index), '.png'),'png')
        
end

function RandomInitialisation(NodeList)
% RANDOMINITIALISATION initialises the uninitialised cells in NodeList,
% giving them random coordinates and angles.
% WARNING! Segments may overlap!

    for m=1:length(NodeList)

        location = 10*rand(1,3)-5;
        angle = 2*pi*rand(1,1);
        NodeList(m).InitialiseSegment( m, angle, 1.0, location );
   
    end
end

function [vec_MinSep, vec_Contact] = GetSeparationAndContactVSL(H1, T1, H2, T2)
% GETSEPARATIONANDCONTACT provides an alternative method for getting
% minumum separation and contact vectors from a pair of line segments, S1
% and S2, respectively defined by vectors to points [H1,T1] and [H2,T2].
 
 % this adapted from SoftSurfers code, where P0==T1 and P1==H1.
 % You can see his derivation at http://geomalgorithms.com/a07-_distance.html

% The object is to calculate sc, tc first and then use these to decide
% which path to return (more on this later when I understand more...)

% ///// DEBUGGED AND TESTED WPJS 20.06.13
% Modified to accomodate variable line length 10.07.13

% PART I: Getting minimum separation vector
% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

u = H1 - T1; % vector for first line
v = H2 - T2; % vector for second line
w = T1 - T2; % separation of the tails

% the key dot products
a = dot(u,u);   % always >= 0
b = dot(u,v);                      
c = dot(v,v);   % always >= 0
d = dot(u,w);
e = dot(v,w);
%============
D = a*c - b*b;  % always >= 0

SMALL_NUM = 0.00000001; % was  0.000000001;

% set default values
sD = D;
tD = D;

% compute line parameters of the two closest points

    %///////////////////////////////////| PARALLEL CASE
    if D < SMALL_NUM % when the lines are almost parallel...
        sN = 0.0;
        tN = e;
        sD = 1.0;
        tD = c;
        % >>>> DEBUG: checked this compartment
        
    %//////////////////////////////////| NON-PARALLEL CASE
    else            %get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        % >>>> DEBUG: checked this compartment
        
        if sN < 0.0       % sc < 0 => the s=0 edge is visible
            sN = 0.0;
            tN = e;
            tD = c;
        % >>>> DEBUG: checked this compartment
        elseif sN > sD      % sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;  
        % >>>> DEBUG: checked this compartment    
        end
        % IF_ HERE Implicit: if 0.0 < sN < sD, none of these cases apply and values
        % of sN, tN remain defined as
        %    sN = (b*e - c*d);
        %    tN = (a*e - b*d);
    end

    %//////////////////////////////| INTEPRET VISIBLE EDGES IN S/T SPACE
    if tN < 0.0      %tc < 0 => the t=0 edge is visible
       tN = 0.0;
       if -d < 0.0  % recompute sc for this edge
           sN = 0.0;
       elseif -d > a
           sN = sD;
       else
           sN = -d;
           sD = a;
       end
    elseif tN > tD        % tc > 1  => the t=1 edge is visible
       tN = tD;
       if (-d + b) < 0.0  % recompute sc for this edge
           sN = 0.0;
       elseif (-d + b) > a
           sN = sD;
       else
           sN = (-d +  b);
           sD = a;
       end
    end
    
    % do the division to get sc and tc
    % =================================
    %sc = (abs(sN) < SMALL_NUM ? 0.0 : sN / sD); % need to replace this operator: it's called a ternary conditional
    %tc = (abs(tN) < SMALL_NUM ? 0.0 : tN / tD); % INSERT <bool> ? <value if true> : <value if false>
    
    % equivalent MatLab statments
    if abs(sN) < SMALL_NUM % choose sc
        sc = 0.0;
    else
        sc = sN / sD;
    end
    if abs(tN) < SMALL_NUM % choose tc
        tc = 0.0;
    else 
        tc = tN / tD;
    end
    
    vec_MinSep = w + (sc * u) - (tc * v);  % =  S1(sc) - S2(tc): points TOWARDS S1
    
% PART II: Getting contact point
% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    if D < SMALL_NUM % check for parallel lines
        
        % if the lines are parallel, evaluate their overlap:
        % overlapping lines form a cuboid of non-zero area, whose centre is
        % P. We want to set the min_VecSep vector so that it passes through P.
        
        N1 = T1 + 0.5*(H1 - T1);      % centre of L1
        N2 = T2 + 0.5*(H2 - T2);      % centre of L2
        
        E = dot(vec_MinSep, u); % check angle between vms and L1
        F = dot(vec_MinSep, v); % check angle between vms and L1
        
        % check to see if the lines overlap. Because the segments are
        % already known to be parallel, you only need to check that
        % separation vector is perpendicular to ONE of the segments.
        
        if abs(E) < SMALL_NUM %&& abs(F) < SMALL_NUM
         
            Length1 = norm(H1 - T1);       % length of the first segment, L1
            Length2 = norm(H2 - T2);       % length of the first segment, L1
            
            % 1. find the longer segment and call it "A". The other segment
            % is "B". Pass over all the relevant information.
             
            if Length2 > Length1 % Segment 2 is longer.
                NA = N2;
                HA = H2;
                TA = T2;
                lenA = Length2;
                
                NB = N1;
                HB = H1;
                TB = T1;
            elseif Length1 >= Length2  % Segment 1 is longer, or they're the same length
                NA = N1;
                HA = H1;
                TA = T1;
                lenA = Length1;
                
                NB = N2;
                HB = H2;
                TB = T2;
            end
            
            unit_p = (HA - TA) / lenA;
            
            % get the vectors from NA to the end points of the smaller
            % segment.
            proj1 = dot(HB - NA, unit_p);
            proj2 = dot(TB - NA, unit_p);
            
            % check how many projections were smaller than half of the
            % longer segment (how much do the segments overlap?)
            
            if abs(proj1) <=  0.5*lenA && abs(proj2) <= 0.5*lenA % complete overlap
                
                % contact must occur at midpoint of smaller segment.
                % By convention, the contact point always lies on line1
                % regardless of which line is bigger: sort this here -
                if NB == N1
                    vec_Contact = NB; 
                elseif NB == N2
                    vec_Contact = NB + vec_MinSep;
                else
                    NEVER_REACHED = 1 % ONE of the two centres must belong to the first line!
                end
               
            else  % incomplete overlap
                
                % here we use the projections of lines from NA to HB,TB to
                % determine the orientation of the second line WRT to the
                % first. Contact vectors can then be written in terms of
                % the smaller of the two projections. 
                
                projections = [proj1,proj2];
                [~,index_min] = min(abs(projections));
                [~,index_max] = max(abs(projections));
                a1 = projections(index_min);
                a2 = projections(index_max);
                
                % vector to the point A1 (from projection)
                A1 = NA + a1*unit_p;
                
                % need vector to the point A1 (from projection)
                
                if a2 > 0.0
                    vec_Contact = HA + 0.5*(A1 - HA);
                else % a2 < 0.0
                     vec_Contact = TA + 0.5*(A1 - TA);
                     % what about when a2 = 0? We defined a2 as being
                     % the larger of the two |projections|, so getting here
                     % requires a1 = 0 also. If a2 = a1 = 0 and the lines
                     % are parallel, then LineB must have length 0.
                end
                
                % again, need to make sure the contact point is on the
                % first segment regardless of size
                if NB == N1
                    vec_Contact = vec_Contact + vec_MinSep; 
                end
            end
            
        % end of parallel and overlapping case    
        %===========================================
        else                                         % parallel but not overlapping case
            vec_Contact = T1 + (sc * u);
        end
        
    else % non-parallel case
         vec_Contact = T1 + (sc * u);
    end % if D < SMALL_NUM
   
end % function

