
classdef Segment < handle
    
    properties
        index           % Analogous to a node index
        vec_centre      % vector to the centre of the segment
        angle           % Angle WRT like [1,0] in radians
        length          % The length of the segment
        vec_force       % The total force acting on the segment
        double_torque   % The total torque acting on the segment
    end 
    
    methods
        
        % Note that individual "Set" methods are superfluous here - there's  no
        % security advantage since the s.method() syntax has clearance.
        
        function InitialiseSegment(Segment, Index, Angle, Length, Centre)
        % INITIALISESEGMENT takes a segment class instance and fills in its
        % spatial attributes with default values.
        Segment.index = Index;
        Segment.angle = Angle;
        Segment.length = Length;
        Segment.vec_centre = Centre;
        Segment.vec_force = [0,0,0];
        Segment.double_torque = 0.0;
        end
        
        function[vec_orientation] = GetOrientationVector(Segment)
        % GETORIENTATIONVECTOR returns a unit orientation vector given a line's
        % angle, measured relative to the line [1,0].
        vec_orientation(1) = cos(Segment.angle);
        vec_orientation(2) = sin(Segment.angle);
        vec_orientation(3) = 0; 
        end
        
        function AddForceAndTorqueContributions(Segment, Force, Torque)
        % ADDFORCEANDTORQUECONTRIBUTION adds vector Force and double Torque
        % to the respective totals of a particular segment.
        Segment.vec_force = Segment.vec_force + Force;
        Segment.double_torque = Segment.double_torque + Torque;
        end
        
        function ResetForceAndTorque(Segment)
        % RESETFORCEANDTORQUE clears the segments total Force and Torque
        % fields.
        Segment.vec_force = [0,0,0];
        Segment.double_torque = 0.0;
        end 
        
    end
end